# VALENCE VET High School Survey Analysis

This project hosts the data collected with the VALENCE VET High School Survey. The Survey was deployed in mid-May 2021, to over 1,000 students of all levels and study profiles in the three partner VET high schools: the Kranj School Centre in Kranj, Slovenia, the  Electrical Engineering School “Mihajlo Pupin” in Novi Sad, Serbia, and the Vocational High School “Ilinden” in Skopje, Macedonia. 

There were a total of 857 responders in the Survey with the majority, 550 participants, coming from Serbia, 170 from Macedonia, and 137 from Slovenia.

The raw data is stored in `csv` files in the folder `raw/`, while the data separated by question is stored in `csv/`.

The analysis was done in the `final_analysis.ipynb` and it was documented in the paper:

- Machine Learning and Data Science Awareness and Experience in Vocational Education and Training High School Students, ETAI 2021 [paper can be found in the `paper_etai/` folder]

And in the paper:
- Data Science and Machine Learning Teaching Practices with Focus on Vocational Education and Training, accepted for publication in the journal Informatics in Education. [preprint in the `paper_journal/` folder]
